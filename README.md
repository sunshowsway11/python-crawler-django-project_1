# Python爬虫+django项目

#### 介绍
使用scrapy框架搭建爬虫项目，爬取豆瓣图书榜TOP250. 在后端进行简单的数据分析处理，然后基于django搭建Web项目进行展示,前端使用HTML + Bootstrap相关组件完成

需要更多框架相关知识点可以去我的csdn博客了解

django学习： https://blog.csdn.net/JYKgo/article/details/112107692

scrapy学习： https://blog.csdn.net/JYKgo/article/details/112062333
#### 使用说明

1.  topbook 为Web项目
2.  douban 为爬虫项目
3.  注意修改两个项目中配置文件中数据库配置
4.  douban/spiders/pipelines.py 文件中是将爬取的数据存储到数据库的部分的代码，可以根据实际更改，比如我这里把爬取的图片的地址存储在了数据库中，而不是直接存储图片数据。
5.  django 命令:
运行python manage.py makemigrations为改动创建迁移记录；
运行python manage.py migrate，将操作同步到数据库
运行django项目:python manage.py runserver 127.0.0.1:8000
6.  scrapy需要的软件包
scrapy爬虫需要的库pypiwin32，lxml，twisted，scrapy，Microsoft Visual C++ 14.0以上 编译环境
数据库连接模块，pymysql，
django框架，数据库操作的库mysqlclient.
各软件包,请自行下载, 附清华镜像源 pip install -i https://pypi.tuna.tsinghua.edu.cn/simple
7. 执行爬虫命令scrapy genspider xxx xxx
8. 爬取的网址https://book.douban.com/top250

#### 项目展示截图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/194955_dabbe110_9141820.png "图片1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/195002_ed545390_9141820.png "图片2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/195010_620f010e_9141820.png "图片3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/195018_ebd35cce_9141820.png "图片4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/195024_bfda6ceb_9141820.png "图片5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/195030_84a9f021_9141820.png "图片6.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/195042_01022b68_9141820.png "图片7.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/195048_358a9944_9141820.png "图片8.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/195054_49c56c71_9141820.png "图片9.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/195100_a5a67c7a_9141820.png "图片10.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/195107_d07c8d7c_9141820.png "图片11.png")